# Backend Typescript Test
***
Este proyecto es una aplicación que gestiona datos relacionados con agricultores, campos, frutas, clientes y cosechas. Proporciona una API RESTful para realizar operaciones CRUD en cada una de estas entidades, así como la capacidad de cargar datos desde archivos CSV.

## Estructura del Proyecto
***

- project
  - index.ts
  - package-lock.json
  - package.json
  - tsconfig.json
  - src
    - controllers
      - customerControllers.ts
      - farmerControllers.ts
      - ...
    - db
      - database.ts
    - models
      - Customer.ts
      - Farmer.ts
      - ...
    - routes
      - customerRoutes.ts
      - farmerRoutes.ts
      - ...
    - services
      - csvService.ts


## Configuración del Proyecto
***
Instalación de dependencias:

```
npm install
```
Ejecución del proyecto:
```
npm start
```

## Funcionalidades Principales
***

### Controladores
> - customerControllers.ts: Gestiona las operaciones CRUD para los clientes.
> - farmerControllers.ts: Gestiona las operaciones CRUD para los agricultores.
Otros controladores para campos, frutas, variedades, cosechas, etc.

### Base de Datos
> database.ts: Configuración de la base de datos SQLite y creación de tablas para agricultores, campos, clientes, etc.

### Modelos
> - Customer.ts: Definición del modelo para los clientes.
> - Farmer.ts: Definición del modelo para los agricultores.
Otros modelos para campos, frutas, variedades, cosechas, etc.

### Rutas
> - customerRoutes.ts: Define las rutas relacionadas con los clientes.
> - farmerRoutes.ts: Define las rutas relacionadas con los agricultores.
Otros archivos de rutas para campos, frutas, variedades, cosechas, etc.

### Servicios
> - csvService.ts: Servicio para cargar datos desde archivos CSV a la base de datos.

## Uso de la API
La API proporciona endpoints para realizar operaciones CRUD en cada entidad. A continuación se muestran algunos ejemplos:

### Clientes:

> - POST /api/customers: Crear un nuevo cliente.
> - GET /api/customers: Obtener todos los clientes.
> - PUT /api/customers/:id Actualizar un cliente existente.
> - DELETE /api/customers/:id Eliminar un cliente.

#### Agricultores, campos, frutas, variedades, cosechas:

Rutas similares a las de los clientes para cada entidad.