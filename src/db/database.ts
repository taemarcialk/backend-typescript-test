import sqlite3 from 'sqlite3';
import path from 'path';

const DB_FILENAME = 'products.db';
const DB_PATH = path.join(__dirname, '..', 'sqlite', DB_FILENAME);
const db = new sqlite3.Database(DB_PATH);

db.serialize(() => {
    db.run(`
        CREATE TABLE IF NOT EXISTS farmers (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT NOT NULL,
            email TEXT NOT NULL UNIQUE
        )
    `);
    db.run(`
        CREATE TABLE IF NOT EXISTS fields (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT NOT NULL,
            location TEXT NOT NULL,
            farmerId INTEGER NOT NULL,
            FOREIGN KEY (farmerId) REFERENCES farmers(id)
        )
    `);

    db.run(`
        CREATE TABLE IF NOT EXISTS fruits (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT NOT NULL UNIQUE
        )
    `);

    db.run(`
        CREATE TABLE IF NOT EXISTS varieties (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT NOT NULL,
            fruitId INTEGER NOT NULL,
            FOREIGN KEY (fruitId) REFERENCES fruits(id)
        )
    `);

    db.run(`
        CREATE TABLE IF NOT EXISTS customers (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT NOT NULL,
            email TEXT NOT NULL UNIQUE
        )
    `);

     db.run(`
        CREATE TABLE IF NOT EXISTS harvests (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            date TEXT NOT NULL,
            quantity INTEGER NOT NULL,
            fieldId INTEGER NOT NULL,
            FOREIGN KEY (fieldId) REFERENCES fields(id)
        )
    `);
});

export default db;
