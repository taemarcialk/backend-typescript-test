import express from 'express';
import farmerRoutes from './routes/farmerRoutes';
import fieldRoutes from './routes/fieldRoutes';
import fruitRoutes from './routes/fruitRoutes';
import varietyRoutes from './routes/varietyRoutes';
import customerRoutes from './routes/customerRoutes';
import harvestRoutes from './routes/harvestRoutes';
import csvRoutes from './routes/csvRoutes';

const app = express();
const PORT = process.env.PORT || 3000;

app.use(express.json());

app.use('/api/farmers', farmerRoutes);

app.use('/api/fields', fieldRoutes);

app.use('/api/fruits', fruitRoutes);

app.use('/api/varieties', varietyRoutes);

app.use('/api/customers', customerRoutes);

app.use('/api/harvests', harvestRoutes);

app.use('/api', csvRoutes);

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});