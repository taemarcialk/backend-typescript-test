import { getManager } from 'typeorm';
import fs from 'fs';
import csvParser from 'csv-parser';
import { Farmer } from '../models/Farmer';
import { Customer } from '../models/Customer';
import { Field } from '../models/Field';
import { Fruit } from '../models/Fruit';
import { Variety } from '../models/Variety';
import { Harvest } from '../models/Harvest';

export const loadCSVData = (filePath: string) => {
    const entityManager = getManager(); 

    fs.createReadStream(filePath)
        .pipe(csvParser())
        .on('data', async (row) => {
            try {
                const farmer = new Farmer(row['Mail Agricultor'], row['Nombre Agricultor'], row['Apellido Agricultor']);
                const customer = new Customer(row['Mail Cliente'], row['Nombre Cliente'], row['Apellido Cliente']);
                const field = new Field(row['Nombre Campo'], row['Ubicación de Campo']);
                const fruit = new Fruit(row['Fruta Cosechada']);
                const variety = new Variety(row['Variedad Cosechada']);

                await entityManager.transaction(async (transactionalEntityManager) => {
                    await transactionalEntityManager.save(farmer);
                    await transactionalEntityManager.save(customer);
                    await transactionalEntityManager.save(field);
                    await transactionalEntityManager.save(fruit);
                    await transactionalEntityManager.save(variety);
                });

                console.log('Datos agregados a la base de datos correctamente.');
            } catch (error) {
                console.error('Error al procesar la fila:', error);
            }
        })
        .on('end', () => {
            console.log('Lectura del archivo CSV completa.');
        });
};

export const loadHarvestData = (filePath: string) => {
    const entityManager = getManager(); 

    fs.createReadStream(filePath)
        .pipe(csvParser())
        .on('data', async (row) => {
            try {
                const harvest = new Harvest(row['Fecha'], parseInt(row['Cantidad']), parseInt(row['IdCampo']));

                await entityManager.transaction(async (transactionalEntityManager) => {
                    await transactionalEntityManager.save(harvest);
                });

                console.log('Datos de cosecha agregados a la base de datos correctamente.');
            } catch (error) {
                console.error('Error al procesar la fila:', error);
            }
        })
        .on('end', () => {
            console.log('Lectura del archivo CSV de cosecha completa.');
        });
};
