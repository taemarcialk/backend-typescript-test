import express from 'express';
import HarvestController from '../controllers/HarvestController';

const router = express.Router();

router.post('/', HarvestController.createHarvest);
router.get('/', HarvestController.getHarvests);
router.put('/:id', HarvestController.updateHarvest);
router.delete('/:id', HarvestController.deleteHarvest);

export default router;
