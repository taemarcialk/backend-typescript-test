import express from 'express';
import CustomerController from '../controllers/CustomerController';

const router = express.Router();

router.post('/', CustomerController.createCustomer);
router.get('/', CustomerController.getCustomers);
router.put('/:id', CustomerController.updateCustomer);
router.delete('/:id', CustomerController.deleteCustomer);

export default router;
