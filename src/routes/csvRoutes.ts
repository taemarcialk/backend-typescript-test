import express, { Request, Response } from 'express';
import { loadCSVData } from '../services/csvService';

const router = express.Router();

router.post('/upload-csv', async (req: Request, res: Response) => {
    const { filePath } = req.body; // Solo necesitas el filePath

    if (!filePath) {
        return res.status(400).json({ error: 'Falta la ruta del archivo CSV.' });
    }

    try {
        await loadCSVData(filePath); // Llama a loadCSVData con un solo argumento
        res.json({ message: 'Archivo CSV cargado exitosamente.' });
    } catch (error) {
        console.error('Error al cargar el archivo CSV:', error);
        res.status(500).json({ error: 'Error al cargar el archivo CSV.' });
    }
});

export default router;
