import express from 'express';
import FarmerController from '../controllers/FarmerController';

const router = express.Router();

router.post('/', FarmerController.createFarmer);
router.get('/', FarmerController.getFarmers);
router.put('/:id', FarmerController.updateFarmer);
router.delete('/:id', FarmerController.deleteFarmer);

export default router;
