import express from 'express';
import FruitController from '../controllers/FruitController';

const router = express.Router();

router.post('/', FruitController.createFruit);
router.get('/', FruitController.getFruits);
router.put('/:id', FruitController.updateFruit);
router.delete('/:id', FruitController.deleteFruit);

export default router;
