import express from 'express';
import VarietyController from '../controllers/VarietyController';

const router = express.Router();

router.post('/', VarietyController.createVariety);
router.get('/', VarietyController.getVarieties);
router.put('/:id', VarietyController.updateVariety);
router.delete('/:id', VarietyController.deleteVariety); 

export default router;
