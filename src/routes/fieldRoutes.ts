import express from 'express';
import FieldController from '../controllers/FieldController';

const router = express.Router();

router.post('/', FieldController.createField);
router.get('/', FieldController.getFields);
router.put('/:id', FieldController.updateField);
router.delete('/:id', FieldController.deleteField);

export default router;
