export class Fruit {
    constructor(
        public name: string,
        public id?: number
    ) {}
}
