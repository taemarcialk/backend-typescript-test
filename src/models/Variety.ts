export class Variety {
    constructor(
        public name: string,
        public id?: number
    ) {}
}
