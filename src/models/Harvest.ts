export class Harvest {
    constructor(
        public date: string,
        public quantity: number,
        public fieldId: number,
        public id?: number
    ) {}
}