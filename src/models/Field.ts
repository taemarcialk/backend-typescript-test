export class Field {
    constructor(
        public name: string,
        public location: string,
        public id?: number
    ) {}
}
