export class Farmer {
    constructor(
        public email: string,
        public name: string,
        public lastName: string,
        public id?: number
    ) {}
}
