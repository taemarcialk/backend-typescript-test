export class Customer {
    constructor(
        public email: string,
        public name: string,
        public lastName: string,
        public id?: number
    ) {}
}
