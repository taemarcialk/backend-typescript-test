import { Request, Response } from 'express';
import db from '../db/database';
import { Farmer } from '../models/Farmer';

const FarmerController = {
    createFarmer: (req: Request, res: Response) => {
        const { email, name, lastName } = req.body; // Desestructura email, name y lastName del cuerpo de la solicitud
        const farmer = new Farmer(email, name, lastName); // Crea una nueva instancia de Farmer con los datos del cuerpo de la solicitud

        db.get('SELECT * FROM farmers WHERE email = ?', [farmer.email], (err, existingFarmer) => {
            if (existingFarmer) {
                return res.status(400).json({ error: 'El correo electrónico ya está en uso.' });
            }
            db.run('INSERT INTO farmers (email, name, lastName) VALUES (?, ?, ?)', [farmer.email, farmer.name, farmer.lastName], function (err) {
                if (err) {
                    return res.status(500).json({ error: 'Error al crear el agricultor.' });
                }
                res.json({ id: this.lastID });
            });
        });
    },
    getFarmers: (req: Request, res: Response) => {
        db.all('SELECT * FROM farmers', (err, farmers: Farmer[]) => {
            if (err) {
                return res.status(500).json({ error: 'Error al obtener los agricultores.' });
            }
            res.json(farmers);
        });
    },
    updateFarmer: (req: Request, res: Response) => {
        const { id, email, name, lastName } = req.body; // Desestructura id, email, name y lastName del cuerpo de la solicitud
        const farmer = new Farmer(email, name, lastName, id); // Crea una nueva instancia de Farmer con los datos del cuerpo de la solicitud

        db.run('UPDATE farmers SET email = ?, name = ?, lastName = ? WHERE id = ?', [farmer.email, farmer.name, farmer.lastName, farmer.id], function (err) {
            if (err) {
                return res.status(500).json({ error: 'Error al actualizar el agricultor.' });
            }
            res.json({ id: this.lastID });
        });
    },

    deleteFarmer: (req: Request, res: Response) => {
        const id = req.params.id;

        db.run('DELETE FROM farmers WHERE id = ?', [id], function (err) {
            if (err) {
                return res.status(500).json({ error: 'Error al eliminar el agricultor.' });
            }
            res.json({ message: 'Agricultor eliminado correctamente.' });
        });
    }
};

export default FarmerController;
