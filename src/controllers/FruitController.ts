import { Request, Response } from 'express';
import db from '../db/database';
import { Fruit } from '../models/Fruit';

const FruitController = {
    createFruit: (req: Request, res: Response) => {
        const fruit: Fruit = req.body;
        const { name } = fruit;

        db.run('INSERT INTO fruits (name) VALUES (?)', [name], function (err) {
            if (err) {
                return res.status(500).json({ error: 'Error al crear la fruta.' });
            }
            res.json({ id: this.lastID });
        });
    },
    getFruits: (req: Request, res: Response) => {
        db.all('SELECT * FROM fruits', (err, fruits: Fruit[]) => {
            if (err) {
                return res.status(500).json({ error: 'Error al obtener las frutas.' });
            }
            res.json(fruits);
        });
    },
        updateFruit: (req: Request, res: Response) => {
        const fruit: Fruit = req.body;
        const { id, name } = fruit;

        db.run('UPDATE fruits SET name = ? WHERE id = ?', [name, id], function (err) {
            if (err) {
                return res.status(500).json({ error: 'Error al actualizar la fruta.' });
            }
            res.json({ id: this.lastID });
        });
    },

    deleteFruit: (req: Request, res: Response) => {
        const id = req.params.id;

        db.run('DELETE FROM fruits WHERE id = ?', [id], function (err) {
            if (err) {
                return res.status(500).json({ error: 'Error al eliminar la fruta.' });
            }
            res.json({ message: 'Fruta eliminada correctamente.' });
        });
    }
};

export default FruitController;
