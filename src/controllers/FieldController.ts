import { Request, Response } from 'express';
import db from '../db/database';
import { Field } from '../models/Field';

const FieldController = {
    createField: (req: Request, res: Response) => {
        const { name, location } = req.body; // Desestructura name y location del cuerpo de la solicitud
        const field = new Field(name, location); // Crea una nueva instancia de Field con los datos del cuerpo de la solicitud

        db.run('INSERT INTO fields (name, location) VALUES (?, ?)', [field.name, field.location], function (err) {
            if (err) {
                return res.status(500).json({ error: 'Error al crear el campo.' });
            }
            res.json({ id: this.lastID });
        });
    },
    getFields: (req: Request, res: Response) => {
        db.all('SELECT * FROM fields', (err, fields: Field[]) => {
            if (err) {
                return res.status(500).json({ error: 'Error al obtener los campos.' });
            }
            res.json(fields);
        });
    },
    updateField: (req: Request, res: Response) => {
        const { id, name, location } = req.body; // Desestructura id, name y location del cuerpo de la solicitud
        const field = new Field(name, location, id); // Crea una nueva instancia de Field con los datos del cuerpo de la solicitud

        db.run('UPDATE fields SET name = ?, location = ? WHERE id = ?', [field.name, field.location, field.id], function (err) {
            if (err) {
                return res.status(500).json({ error: 'Error al actualizar el campo.' });
            }
            res.json({ id: this.lastID });
        });
    },

    deleteField: (req: Request, res: Response) => {
        const id = req.params.id;

        db.run('DELETE FROM fields WHERE id = ?', [id], function (err) {
            if (err) {
                return res.status(500).json({ error: 'Error al eliminar el campo.' });
            }
            res.json({ message: 'Campo eliminado correctamente.' });
        });
    }
};

export default FieldController;
