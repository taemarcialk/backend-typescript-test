import { Request, Response } from 'express';
import db from '../db/database';
import { Harvest } from '../models/Harvest';

const HarvestController = {
    createHarvest: (req: Request, res: Response) => {
        const harvest: Harvest = req.body;
        const { date, quantity, fieldId } = harvest;

        db.run('INSERT INTO harvests (date, quantity, fieldId) VALUES (?, ?, ?)', [date, quantity, fieldId], function (err) {
            if (err) {
                return res.status(500).json({ error: 'Error al crear la cosecha.' });
            }
            res.json({ id: this.lastID });
        });
    },
    getHarvests: (req: Request, res: Response) => {
        db.all('SELECT * FROM harvests', (err, harvests: Harvest[]) => {
            if (err) {
                return res.status(500).json({ error: 'Error al obtener las cosechas.' });
            }
            res.json(harvests);
        });
    },
        updateHarvest: (req: Request, res: Response) => {
        const harvest: Harvest = req.body;
        const { id, date, quantity, fieldId } = harvest;

        db.run('UPDATE harvests SET date = ?, quantity = ?, fieldId = ? WHERE id = ?', [date, quantity, fieldId, id], function (err) {
            if (err) {
                return res.status(500).json({ error: 'Error al actualizar la cosecha.' });
            }
            res.json({ id: this.lastID });
        });
    },

    deleteHarvest: (req: Request, res: Response) => {
        const id = req.params.id;

        db.run('DELETE FROM harvests WHERE id = ?', [id], function (err) {
            if (err) {
                return res.status(500).json({ error: 'Error al eliminar la cosecha.' });
            }
            res.json({ message: 'Cosecha eliminada correctamente.' });
        });
    }
};

export default HarvestController;
