import { Request, Response } from 'express';
import db from '../db/database';
import { Variety } from '../models/Variety';

const VarietyController = {
    createVariety: (req: Request, res: Response) => {
        const { name } = req.body; // Desestructura name del cuerpo de la solicitud
        const variety = new Variety(name); // Crea una nueva instancia de Variety con el nombre del cuerpo de la solicitud

        db.run('INSERT INTO varieties (name) VALUES (?)', [variety.name], function (err) {
            if (err) {
                return res.status(500).json({ error: 'Error al crear la variedad.' });
            }
            res.json({ id: this.lastID });
        });
    },
    getVarieties: (req: Request, res: Response) => {
        db.all('SELECT * FROM varieties', (err, varieties: Variety[]) => {
            if (err) {
                return res.status(500).json({ error: 'Error al obtener las variedades.' });
            }
            res.json(varieties);
        });
    },
    updateVariety: (req: Request, res: Response) => {
        const { id, name } = req.body; // Desestructura id y name del cuerpo de la solicitud
        const variety = new Variety(name, id); // Crea una nueva instancia de Variety con los datos del cuerpo de la solicitud

        db.run('UPDATE varieties SET name = ? WHERE id = ?', [variety.name, variety.id], function (err) {
            if (err) {
                return res.status(500).json({ error: 'Error al actualizar la variedad.' });
            }
            res.json({ id: this.lastID });
        });
    },

    deleteVariety: (req: Request, res: Response) => {
        const id = req.params.id;

        db.run('DELETE FROM varieties WHERE id = ?', [id], function (err) {
            if (err) {
                return res.status(500).json({ error: 'Error al eliminar la variedad.' });
            }
            res.json({ message: 'Variedad eliminada correctamente.' });
        });
    }
};

export default VarietyController;
