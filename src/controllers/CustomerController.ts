import { Request, Response } from 'express';
import db from '../db/database';
import { Customer } from '../models/Customer';

const CustomerController = {
    createCustomer: (req: Request, res: Response) => {
        const customer: Customer = req.body;
        const { name, email } = customer;

        db.run('INSERT INTO customers (name, email) VALUES (?, ?)', [name, email], function (err) {
            if (err) {
                return res.status(500).json({ error: 'Error al crear el cliente.' });
            }
            res.json({ id: this.lastID });
        });
    },
    getCustomers: (req: Request, res: Response) => {
        db.all('SELECT * FROM customers', (err, customers: Customer[]) => {
            if (err) {
                return res.status(500).json({ error: 'Error al obtener los clientes.' });
            }
            res.json(customers);
        });
    },
        updateCustomer: (req: Request, res: Response) => {
        const customer: Customer = req.body;
        const { id, name, email } = customer;

        db.run('UPDATE customers SET name = ?, email = ? WHERE id = ?', [name, email, id], function (err) {
            if (err) {
                return res.status(500).json({ error: 'Error al actualizar el cliente.' });
            }
            res.json({ id: this.lastID });
        });
    },

    deleteCustomer: (req: Request, res: Response) => {
        const id = req.params.id;

        db.run('DELETE FROM customers WHERE id = ?', [id], function (err) {
            if (err) {
                return res.status(500).json({ error: 'Error al eliminar el cliente.' });
            }
            res.json({ message: 'Cliente eliminado correctamente.' });
        });
    }
};

export default CustomerController;
